import React, {Component} from 'react';
import TodoListItem from './todo_list_item';

class TodoList extends Component {

  constructor (props) {
    super(props);
  }

  render() {

    return (
      <div>
        <ul>
          {this.props.items.map(item => (
              <TodoListItem
                key={item.id}              
                handleSubmit={this.props.handleSave}
                handleTextEdit={this.props.handleTextEdit}
                handleRemove={this.props.handleRemove}
                handleSave={this.props.handleSave}
                value={this.props.value}
                item={item}
                isDone={this.props.isDone}
              />
           ) )
          }
        </ul>
      </div>
);
};
};

export default TodoList;
