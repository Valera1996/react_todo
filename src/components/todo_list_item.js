import React, {Component} from 'react';

class TodoListItem extends Component  {


 constructor (props){
   super(props);
   this.handleSave = this.handleSave.bind(this);
   this.toggleEditState = this.toggleEditState.bind(this);
   this.handleTextEdit = this.handleTextEdit.bind(this);
   this.state={
     isEditing: true
   }
 }

handleSave(item) {
  let value = this.input.value;
  item.textValue=value;
  this.props.handleSave(item);
}

toggleEditState () {
    this.setState ( ( {
      isEditing: !this.state.isEditing
    }  ));

};

handleTextEdit(e) {
    console.log(e.target.value);
};

render()  {
            return (
              <li className={this.props.item.isDone ? 'text-strike' : null } >
                {this.state.isEditing
                  ?<div>
                    <input type="checkbox" defaultChecked={this.props.item.isDone } onChange={() => this.props.isDone(this.props.item) } />
                    { this.props.item.text }
                    <button onClick={() => this.toggleEditState()}>edit</button>
                    <button onClick={() => this.props.handleRemove(this.props.item.id)}>del</button>
                   </div>
                  :<div>
                    <form >
                      <input defaultValue={this.props.item.text} ref={(i) => { this.input = i; }} />
                      <button type="button" onClick={() =>{ this.toggleEditState() ; this.handleSave(this.props.item); }} > Save </button>
                    </form>
                   </div> }
              </li>
              )
    }
};

export default TodoListItem;
