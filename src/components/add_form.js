import React, {Component} from 'react';

const AddForm = (props) => {
  return (
    <div>
      <h3>List of TODOs</h3>
      <form onSubmit={props.handleSubmit}>
        <input onChange={props.handleChange} value={props.value.text} />
        <button>{'Add #' + (props.value.items.length + 1)}</button>
      </form>
    </div>
  )
}

export default AddForm;
