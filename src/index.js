import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TodoList from './components/todo_list';
import AddForm from './components/add_form';

  class TodoApp extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleRemove = this.handleRemove.bind(this);
      this.handleTextEdit = this.handleTextEdit.bind(this);
      this.handleSave = this.handleSave.bind(this);
      this.isDone = this.isDone.bind(this);
      this.state = {items: [
                      {  text: 'first work that needs to be done',  id: 1, isDone: false },
                      {  text: 'second work that needs to be done',  id: 2, isDone: false },
                      {  text: 'third work that needs to be done',  id: 3, isDone: true }      ],
                      text: '',
                      value: ''
                   }; }

      handleRemove (id) {
        let  newArr = this.state.items.filter(function( obj ) {
            return obj.id !== id;
        });
        this.setState({
          items: newArr
        });
      };



      isDone (item)  {
        let newArr = this.state.items;
        var foundIndex = newArr.findIndex(x => x.id == item.id );
          if(item.isDone){
        item.isDone=false;
        }
          else {
          item.isDone=true;
        }
        newArr[foundIndex] = item;
            this.setState({
            items: newArr
        });
      };

    handleChange(e) {
      console.log(e.target.value);
      this.setState({text: e.target.value});
    };

    handleTextEdit(e) {
        console.log(e.target.value);
        this.setState({value: e.target.value});
    };

    handleSave(item) {

      console.log(item);

      let newArr = this.state.items;
      var foundIndex = newArr.findIndex(x => x.id == item.id );
      newArr[foundIndex].text = item.textValue;
      this.setState({
          items: newArr
      });
    }

    handleSubmit(e) {
      e.preventDefault();
      var newItem = {
        text: this.state.text,
        id: Date.now(),
        isDone: false
      };
      this.setState((prevState) => ({
        items: prevState.items.concat(newItem),
        text: ''
      }));
    };

    render() {
      return (
        <div>
          <AddForm handleSubmit={this.handleSubmit} handleChange={this.handleChange} value={this.state} />
          <TodoList items={this.state.items} value={this.state} handleRemove={this.handleRemove}
          isDone={this.isDone} handleTextEdit={this.handleTextEdit} handleSave={this.handleSave}
          handleChange={this.handleChange} handleEdit={this.handleEdit}/>
        </div>
      );
    }
  }
    ReactDOM.render(<TodoApp />, document.querySelector('.container'));
